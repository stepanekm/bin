all: leader

leader: leader.cpp ca_grid.cpp population.cpp individual.cpp
	g++ -O3 -march=native -fopenmp --std=c++17 -Wall -Wextra -pedantic leader.cpp ca_grid.cpp population.cpp individual.cpp -o leader

debug:
	g++ -g -O0 -Wall -Wextra -pedantic leader.cpp ca_grid.cpp population.cpp -o leader
