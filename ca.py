import random

rules = 0b11111011101111101110111011011011111110111100001010111110001111101110010111000111011011111010001011111010001110101001001001000000
steps = 149*2

def get_val(g, r, i):
    index = int(g[i - 3 : i + 4], 2)
    return "1" if (rules >> index) & 1 else "0"

successes = 0
all = 0

for _ in range(100):
    grid = ''.join(random.choices(['1' for i in range(100)] + ['0'], k=149))
    grid = grid[-3:] + grid + grid[:3]
    for _ in range(steps):
        tmp = [None for _ in range(len(grid) - 6)]
        for i in range(3, len(grid) - 3):
            tmp[i - 3] = get_val(grid, rules, i)
        start = tmp[:3]
        end = tmp[-3:]
        grid = end + tmp + start
        grid = "".join(grid)
    print(grid)
    all += 1
    grid = grid[3:-3]
    successes = successes + 1 if grid.count('1') == 1 else successes

print(successes * 100 / all, "%")
