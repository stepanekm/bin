#include <bitset>
#include <cstring>
#include <iostream>
#include <random>

#include "ca_grid.h"

CAGrid::CAGrid(int size) : size(size)
{
    int sizeWithPadding = size + 6;
    sizeInBytes = (sizeWithPadding - 1) / 8 + 1;
    if(sizeInBytes == 1)
        sizeInBytes = 2;
    lastButOneByteIdx = sizeInBytes - 2;
    lastBitIndex = size + 2 - 8 * lastButOneByteIdx;

    grid = new unsigned char[sizeInBytes];
    tmpGrid = new unsigned char[sizeInBytes];

    std::uint64_t* grid64Bit = reinterpret_cast<std::uint64_t*>(grid);

    std::random_device rd;
    std::mt19937_64 mt(rd());

    std::uniform_int_distribution<std::uint64_t> dist{};

    // Generate random grid
    for(int i = 0; i < (sizeInBytes + 7) / 8; ++i)
    {
        grid64Bit[i] = dist(mt);
    }

    exchangeEdges();
}


CAGrid::CAGrid(const CAGrid& rhs) : size(rhs.size), sizeInBytes(rhs.sizeInBytes),
        lastButOneByteIdx(rhs.lastButOneByteIdx), lastBitIndex(rhs.lastBitIndex)
{
    grid = new unsigned char[sizeInBytes];
    tmpGrid = new unsigned char[sizeInBytes];
    std::memcpy(grid, rhs.grid, sizeInBytes);
}


CAGrid::~CAGrid()
{
    delete[] grid;
    delete[] tmpGrid;
}


/**
 * Exchanges edges of the grid, so that the grid behaves like toroid
 */
void CAGrid::exchangeEdges()
{
    // First 3 bits (not considering 3-bit padding)
    int first3bits = (grid[0] >> 2) & 7;

    // Swap bytes in order to work correctly with 16-bit value on little-endian
    std::swap(grid[lastButOneByteIdx], grid[lastButOneByteIdx + 1]);

    // We want to access last 3 (non-padding) bits. Those bits can be either in the last byte,
    // in last-but-one byte or on edge between those two. To make thing easier, we work with
    // last two bytes as with one 16-bit value
    std::uint16_t* lastTwoBytes = reinterpret_cast<std::uint16_t*>(grid + lastButOneByteIdx);

    // Last 3 bits (not considering 3-bit padding)
    std::uint64_t last3bits = ((*lastTwoBytes) >> (15 - lastBitIndex)) & 7;

    // Clear last 3 (padding) bits
    *lastTwoBytes &= (~0UL) << (15 - lastBitIndex);
    // Set value of last 3 (padding) bits to value of first 3 (non-padding) bits
    *lastTwoBytes |= first3bits << (12 - lastBitIndex);

    // Swap bytes back
    std::swap(grid[lastButOneByteIdx], grid[lastButOneByteIdx + 1]);

    // Clear first 3 (padding) bits
    grid[0] &= 0b00011111;
    // Set value of first 3 (padding) bits to value of last 3 bits
    grid[0] |= last3bits << 5;
}


bool CAGrid::evaluate(int steps, const Individual& individual)
{
    auto transitionFunction = individual.getTable();
    for(int step = 0; step < steps; ++step)
    {
        std::memset(tmpGrid, 0, sizeInBytes);
        int index = 0;
        for(int byteIdx = 0; byteIdx < sizeInBytes; byteIdx++)
        {
            index = (index & 0b111000) << 8;
            index |= grid[byteIdx] << 3;
            index |= byteIdx != sizeInBytes - 1 ? (grid[byteIdx + 1] >> 5) & 0b111 : 0;
            tmpGrid[byteIdx] = transitionFunction[index];
        }
        std::swap(grid, tmpGrid);
        exchangeEdges();
    }

    // Count number of 1s. If there is one 1, return true, otherwise return false.
    int ones = 0;
    int bits = 0;
    for(int i = 0; i < sizeInBytes; ++i)
    {
        for(int j = 7; j >= 0; --j)
        {
            ones += (grid[i] >> j) & 1;
            if(ones > 1)
                return false;

            bits++;
            if(bits >= size)
                return ones == 1;
        }
    }
    return ones == 1;
}
