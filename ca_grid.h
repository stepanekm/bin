#ifndef CA_GRID_H
#define CA_GRID_H

#include "population.h"

class CAGrid
{
public:
    CAGrid(int size);
    CAGrid(const CAGrid& rhs);
    ~CAGrid();
    bool evaluate(int steps, const Individual& individual);

private:
    void exchangeEdges();
    inline void swapLastTwoBytes();

    unsigned char* grid;
    unsigned char* tmpGrid;
    int size;
    int sizeInBytes;
    int lastButOneByteIdx;
    int lastBitIndex;
};

#endif
