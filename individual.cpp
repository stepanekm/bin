#include <iostream>
#include <algorithm>
#include <fstream>
#include <random>
#include <string>
#include <iostream>
#include <omp.h>

#include "individual.h"
#include "ca_grid.h"


Individual::Individual(chromosomeT ch) : ch(ch), fitness(0)
{
    fillTable();
}


Individual::Individual(const std::string& s) : fitness(0)
{
    // Transforms string to 128 bit chromosome
    std::string firstPart = s.substr(0,64);
    std::string secondPart = s.substr(64, 128);
    char* endptr = NULL;
    std::uint64_t higherBits = strtoull(firstPart.c_str(), &endptr, 2);
    std::uint64_t lowerBits = strtoull(secondPart.c_str(), &endptr, 2);
    ch = static_cast<chromosomeT>(higherBits) << 64 | lowerBits;
    fillTable();
}


/** Fills lookup table of individual.
 *  THe table is used to evaluate 8 cells of cellular automata at once
 */
void Individual::fillTable()
{
    for(int byteIdx = 0; byteIdx < 16384; ++byteIdx)
    {
        for(int bitIndex = 0; bitIndex < 8; ++bitIndex)
            table[byteIdx] |= ((ch >> ((byteIdx >> bitIndex) & 0b01111111)) & 1) << bitIndex;
    }
}


/** Computes fitness on randomly generated cellular automata grids
 */
int Individual::computeFitness(int grids, int CASize)
{
    fitness = 0;
    for(int i = 0; i < grids; ++i)
        fitness += CAGrid(CASize).evaluate(2 * CASize, *this);

    return fitness;
}


std::string Individual::toStr() const
{
    std::string str;
    chromosomeT chTmp = ch;
    for(int i = 0; i < 128; ++i)
    {
        char c = (chTmp & 1) ? '1' : '0';
        chTmp >>= 1;
        str = c + str;
    }
    return str;
}


chromosomeT Individual::getChromosome() const
{
    return ch;
}


int Individual::getFitness() const
{
    return fitness;
}


void Individual::setFitness(int newFitness)
{
    fitness = newFitness;
}


bool Individual::operator==(const Individual& r)
{
    return ch == r.ch;
}


const unsigned char* Individual::getTable() const
{
    return table;
}
