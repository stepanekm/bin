#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include <string>
#include <vector>

using chromosomeT = unsigned __int128;


class Individual {
public:
    Individual(chromosomeT ch);
    Individual(const std::string& s);
    void fillTable();
    std::string toStr() const;
    chromosomeT getChromosome() const;
    int getFitness() const;
    void setFitness(int newFitness);
    bool operator==(const Individual& r);
    const unsigned char* getTable() const;
    int computeFitness(int grids, int CASize);

private:
    chromosomeT ch;
    int fitness;
    unsigned char table[16384] = {0};
};


#endif
