#include <iostream>
#include <random>

#include "ca_grid.h"
#include "population.h"


void generateIndividuals(size_t CASize)
{
    #pragma omp parallel
    {
        std::random_device rd;
        std::mt19937_64 mt(rd());

        std::uniform_int_distribution<std::uint64_t> dist{};
        while(true)
        {
            // Create random individual
            Individual individual{(static_cast<chromosomeT>(dist(mt)) << 64) | dist(mt)};

            // Optimization - good chromozomes never have 1 on the first pozition. That would mean
            // that cell with neighbourhood 000-0-000 would go to state 1, which is not good behavior
            // for leader election problem.
            if(individual.getChromosome() & 1)
                continue;

            individual.computeFitness(100, CASize);

            // Individual must be better than some threshold
            if(individual.getFitness() >= 20)
            {
                #pragma omp critical
                std::cout << individual.toStr() << "\n";
            }
        }
    }
}


int main(int argc, char* argv[])
{
    size_t experiments = 500;
    size_t generations = 1000;
    size_t populationSize = 12;
    size_t CASize = 149;

    // ./leader -v <transition_function> : verifies fitness of individual
    // on a huge number of random grids.
    if(argc > 2 && std::string{"-v"}.compare(argv[1]) == 0)
    {
        std::string ch{argv[2]};
        Individual individual{ch};
        int numGrids = 100000;
        individual.computeFitness(numGrids, CASize);
        std::cout << "Success rate: " << individual.getFitness() * 100.0 / numGrids << " %\n";
        return 0;
    }
    // ./leader -g : Generates individuals that can serve as initializations for future runs
    // Runs until the program is killed.
    else if(argc > 1 && std::string{"-g"}.compare(argv[1]) == 0)
    {
        generateIndividuals(CASize);
        return 0;
    }


    // If no argument is set, run experiments
    #pragma omp parallel for
    for(size_t exp = 0; exp < experiments; ++exp)
    {
        Population population{populationSize, CASize};
        population.loadInitialIndividuals();
        for(size_t gen = 0; gen < generations; ++gen)
        {
            population.evaluateIndividuals();
            population.evaluateProgress(gen);
            population.generateNewGeneration();
        }

        int realBestFitness = population.getBestIndividual().computeFitness(100000, CASize);
        std::cout << "Fitness " << realBestFitness / 1000.0 << ", function: " << population.getBestIndividual().toStr() << "\n";



        /** Part of code for generating best solutions
        *
        * // If the population seems promising, evolve it in more generations
        * if(population.getMaxFitness() > 95)
        * {
        *     for(size_t gen = generations; gen < 5 * generations; ++gen)
        *     {
        *         population.evaluateIndividuals();
        *         population.evaluateProgress(gen);
        *         population.generateNewGeneration();
        *     }

        * }

        * // Fitness of individuals is usually higher than real fitness computed on higher number
        * // of random grids => compute fitness again on new grids.
        * int realBestFitness = population.getBestIndividual().computeFitness(1000, CASize);

        * if(realBestFitness > 990)
        * {
        *     for(size_t gen = 5 * generations; gen < 20 * generations; ++gen)
        *     {
        *         population.evaluateIndividuals();
        *         population.evaluateProgress(gen);
        *         population.generateNewGeneration();
        *     }
        *     realBestFitness = population.getBestIndividual().computeFitness(100000, CASize);
        *     #pragma omp critical
        *     std::cout << "Experiment " << exp << ": fitness " << realBestFitness / 1000.0 << ", function: " << population.getBestIndividual().toStr() << "\n";
        * }
        */

    }
}
