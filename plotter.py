import matplotlib
import random
matplotlib.use('TKAgg')

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.animation as animation

N    = 149
rows_to_display = int(N * 2)
rule = 0b11111111111011001110111010000111111101111110111111101110110000101111110000100100001111000000100000100010010000000010011000001000

def init(n=N, randrow=True):
    grid = np.zeros(shape=(2*n,n), dtype=np.int32)
    grid[0] = np.array(random.choices([1 for i in range(1)] + [0], k=n), dtype=np.int32)
    return grid


def update(d):
    global row
    data = grid[row]
    data = np.concatenate((data[-3:], data, data[:3]))
    row += 1
    if row >= 2 * N:
        return
    for i in range(len(grid[row])):
        grid[row][i] = get_val(data, i + 3)
    start = max(row - rows_to_display, 0)
    mat.set_data(grid[start:start+rows_to_display])
    return mat


def get_val(g, i):
    index = "".join([str(x) for x in g[i - 3 : i + 4]])
    index = int(index, 2)
    return 1 if (rule >> index) & 1 else 0


fig, ax = plt.subplots()
ax.axis('off')
row = 0
grid = init()
mat = ax.matshow(grid[0:rows_to_display], cmap=cm.Greens, interpolation='spline16')
ani = animation.FuncAnimation(fig, update, frames=2*N - 1, interval=1, save_count=200, repeat=False)
#ani.save('rule90.mp4', fps=30)
plt.show()
