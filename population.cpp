#include <iostream>
#include <algorithm>
#include <fstream>
#include <random>
#include <string>
#include <omp.h>

#include "population.h"
#include "ca_grid.h"


Population::Population(size_t size, size_t CASize) : size(size), CASize(CASize), bestIndividual(0)
{
    individuals.reserve(size);
}


/** Reads file `input.txt` and creates population from randomly selected chromosoms from that file
*/
void Population::loadInitialIndividuals()
{
    std::ifstream in("input.txt");
    std::vector<std::string> lines;
    std::string line;
    while((in >> line) && !in.eof())
        lines.push_back(line);

    std::random_device rd;
    std::mt19937 mt(rd());
    for(size_t i = 0; i < size; ++i)
    {
        std::uniform_int_distribution<size_t> dist{0, lines.size() - 1};
        int lineIndex = dist(mt);
        individuals.emplace_back(lines[lineIndex]);
        lines.erase(lines.begin() + lineIndex);
    }
}


/** Evaluates if best individual changed
*/
void Population::evaluateProgress(int generation)
{
    bool bestFitnessChanged = false;
    for(auto& individual : individuals)
    {
        if(individual.getFitness() > bestIndividual.getFitness())
        {
            bestIndividual = individual;
            bestFitnessChanged = true;
        }
    }
    //if(bestFitnessChanged)
    //    std::cout << "Generation " << generation << ": fitness " << bestIndividual.getFitness() << ", function: " << bestIndividual.toStr() << "\n";
}


/** Creates new individual by performing crossover between two parents.
 *  Parents are chosen in small tournaments between two randomly chosen individuals.
 */
chromosomeT Population::createChild() const
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<size_t> parentsDist{0, size - 1};

    auto parent1Candidate1 = individuals[parentsDist(mt)];
    auto parent1Candidate2 = individuals[parentsDist(mt)];
    auto parent2Candidate1 = individuals[parentsDist(mt)];
    auto parent2Candidate2 = individuals[parentsDist(mt)];

    auto parent1 = (parent1Candidate1.getFitness() > parent1Candidate2.getFitness()) ? parent1Candidate1.getChromosome() : parent1Candidate2.getChromosome();
    auto parent2 = (parent2Candidate1.getFitness() > parent2Candidate2.getFitness()) ? parent2Candidate1.getChromosome() : parent2Candidate2.getChromosome();

    bool uniformCrossover = true;
    if(uniformCrossover)
    {
        std::uniform_int_distribution<std::uint32_t> crossoverDist{0, 1};
        chromosomeT child = 0;
        for(int i = 0; i < 128; ++i)
        {
            child |= crossoverDist(mt) ? (parent1 & (static_cast<chromosomeT>(1) << i)) :
                                       (parent2 & (static_cast<chromosomeT>(1) << i));
        }
        return child;
    }

    std::uniform_int_distribution<std::uint32_t> crossoverDist{0, 128};

    int crossoverStart = crossoverDist(mt);
    int crossoverEnd = crossoverDist(mt);
    if(crossoverStart > crossoverEnd)
        std::swap(crossoverStart, crossoverEnd);
    if(crossoverEnd - crossoverStart < 2)
    {
        if(crossoverStart > 2)
            crossoverStart -= 2;
        else
            crossoverEnd += 2;
    }

    chromosomeT crossoverMask = ((static_cast<chromosomeT>(1) << (crossoverEnd - crossoverStart)) - 1) << crossoverStart;
    chromosomeT child = (parent1 & (~crossoverMask)) | (parent2 & crossoverMask);
    return child;
}


/** Randomly toggles one bit in chromosome
 */
chromosomeT Population::mutate(chromosomeT ch) const
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<std::uint32_t> dist{0, 127};

    ch ^= static_cast<chromosomeT>(1) << dist(mt);
    return ch;
}


/** Generates new generation.
 */
void Population::generateNewGeneration()
{
    // Individuals are sorted by fitness.
    std::sort(individuals.begin(), individuals.end(), [this](const Individual& l, const Individual& r)
                                {
                                    return l.getFitness() > r.getFitness();
                                });

    std::vector<Individual> newIndividuals;

    // First n different individuals is copied into new generation (elitism)
    for(size_t i = 0; i < individuals.size(); ++i)
    {
        if(individuals[i].getFitness() > 0 && std::find(newIndividuals.begin(), newIndividuals.end(), individuals[i]) == newIndividuals.end())
        {
            newIndividuals.push_back(individuals[i]);
            if(newIndividuals.size() >= size / 6)
                break;
        }
    }

    // Rest of new individuals is created by crossovers and mutation of old individuals.
    for(size_t i = newIndividuals.size(); i < size; ++i)
    {
        auto child = createChild();
        child = mutate(child);
        newIndividuals.emplace_back(child);
    }

    individuals = newIndividuals;
}


/** Evaluates all individuals
 */
void Population::evaluateIndividuals()
{
    for(size_t i = 0; i < individuals.size(); ++i)
        individuals[i].computeFitness(100, CASize);
}


int Population::getMaxFitness() const
{
    return bestIndividual.getFitness();
}


Individual Population::getBestIndividual() const
{
    return bestIndividual;
}
