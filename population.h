#ifndef POPULATION_H
#define POPULATION_H

#include <string>
#include <vector>

#include "individual.h"

using chromosomeT = unsigned __int128;

class Population
{
public:
    Population(size_t populationSize, size_t CASize);
    void loadInitialIndividuals();
    void evaluateIndividuals();
    void generateNewGeneration();
    void evaluateProgress(int generation);
    int getMaxFitness() const;
    Individual getBestIndividual() const;

private:
    chromosomeT createChild() const;
    chromosomeT mutate(chromosomeT individual) const;

    std::vector<Individual> individuals;
    size_t size;     // Number of individuals
    size_t CASize;   // Size of cellular automata
    Individual bestIndividual;
};


#endif
